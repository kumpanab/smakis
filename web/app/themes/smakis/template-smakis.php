<?php
/*
Template Name: Smakis template
*/

?>

<style>
  img{
    max-width: 100%;
    height: auto;
  }
</style>

<section>
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
        <?php while (have_posts()) : the_post(); ?>
          <?php the_content(); ?>
        <?php endwhile; ?>
			</div>
		</div>
	</div>
</section>

