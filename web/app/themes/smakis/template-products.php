<?php
/*
Template Name: Products template
*/

?>

<?php if( have_rows('product_category') ): ?>
	<?php while( have_rows('product_category') ): the_row(); $i = 0;?>
		<section id="product-category-top">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<h2><?php the_sub_field('title'); ?></h2>
						<p><?php the_sub_field('text'); ?></p>
					</div>
				</div>
			</div>
		</section>
		<section>
			<div class="container product-category">
				<?php if( have_rows('products') ): ?>
					<?php while( have_rows('products') ): the_row();
						$infoColor = get_sub_field('sub_category_color');
					?>	
						<div class="row">
							<div class="col-xs-12">
								<?php if (get_sub_field('sub_category_title') != ''): ?>
									<h3 <?php if($i == 0){echo 'style="margin-top: 0px;"';$i = 1;} ?>><?php the_sub_field('sub_category_title'); ?></h3>
								<?php endif ?>
							</div>
							<?php if( have_rows('sub_category') ): ?>
								<?php while( have_rows('sub_category') ): the_row();
									$productImage = get_sub_field('product_image');
								?>							
									<div class="col-lg-2 col-md-3 col-sm-4 col-xs-6">
										<div class="product-container">
											<div class="product-info" style="background-color: #<?php echo $infoColor; ?> ;">
											<div class="info-container">
													<p><span><?php the_sub_field('info_text'); ?></span></p>
													<?php if (get_sub_field('info_link_sheet') == ''): ?>
														<p class="no-product-link"><span>-</span></p>
													<?php else: ?>
														<a href="<?php the_sub_field('info_link_sheet'); ?>" target="_blank"><span>länk till produktblad</span></a>
													<?php endif ?>
													<?php if (get_sub_field('info_link_image') == ''): ?>
														<p class="no-product-link"><span>-</span></p>
													<?php else: ?>
														<a href="<?php the_sub_field('info_link_image'); ?>" target="_blank"><span>ladda ner bild</span><span class="glyphicon glyphicon-arrow-down"></span></a>
													<?php endif ?>
											</div>
													<div class="arrow-down" style="border-top: 10px solid #<?php echo $infoColor ?> ;"></div>
											</div>
											<img src="<?php echo $productImage['url']; ?>">
											<p><?php the_sub_field('row_1'); ?>&nbsp;</p>
											<p class="product-row-two"><?php the_sub_field('row_2'); ?>&nbsp;</p>
										</div>
									</div>
								<?php endwhile; ?>
							<?php endif; ?>
						</div>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</section>
	<?php endwhile; ?>
<?php endif; ?>

<section id="campaign">
	<?php $buttonFieldImage = get_field('campaign_image'); ?>
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-push-2 col-xs-12 col-xs-push-0">
				<h2><?php the_field('campaign_title'); ?></h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-md-push-2 col-xs-12 col-xs-push-0">
				<p><?php the_field('campaign_text'); ?></p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-md-push-2 col-xs-12 col-xs-push-0">
				<img src="<?php echo $buttonFieldImage['url']; ?>">
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
					<a class="button" href="<?php the_field('campaign_button_link'); ?>"><?php the_field('campaign_button_text'); ?></a>
			</div>
		</div>
	</div>
</section>