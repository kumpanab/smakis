<footer class="content-info" role="contentinfo">
  <?php $backgroundStyle = '';if (!is_page_template('template-plantera.php')): ?>
  	<section id="top">
  		<div class="container">
  			<div class="row">
  				<div class="col-xs-12">
  					<div class="top-container">
  						<img src="<?php bloginfo('template_url'); ?>/assets/img/footer/vi_har_tre_kompisar.png">
  						<!-- <img id="apple" src="<?php bloginfo('template_url'); ?>/assets/img/footer/footer_halsning_apple.png"> -->
  					</div>
  				</div>
  			</div>
  		</div>
  	</section>
  <?php else: ?>
    <?php $backgroundStyle = 'style="background-color: #f5f5f5"'; ?>
  <?php endif ?>
	<section id="bottom" <?php echo $backgroundStyle ?>>
		<div class="image-map">
			<a href="tel:+4687740475"><div class="footer-icon-link"></div></a>
			<a href="https://goo.gl/maps/dVPAQ" target="_blank"><div class="footer-icon-link"></div></a>
			<a href="mailto:info@smakis.se"><div class="footer-icon-link"></div></a>
		</div>
		<p>Kalasföretaget Säljprofilen AB</p>
		<p>Box 1170</p>
		<p>131 27 Nacka Strand</p>
		<p>Besöksadress: Augustendalsvägen 64</p>
		<p>Växel: 08-774 04 75</p>
		<p>Fax: 08-774 23 70</p>
		<a href="mailto:info@smakis.se"><p>info@smakis.se</p></a>
	</section>

</footer>

<?php wp_footer(); ?>
