<div id="header-wrapper">
  <header class="banner navbar navbar-default navbar-static-top" role="banner">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <!--<a class="navbar-brand" href="<?php echo home_url(); ?>/"><?php bloginfo('name'); ?></a>-->
      </div>

      <nav class="collapse navbar-collapse" role="navigation">
        <?php
          if (has_nav_menu('primary_navigation')) :
            wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav'));
          endif;
        ?>
        
        <a href="http://instagram.com/smakis" target="_blank">
          <div class="icon-container instagram"></div>
        </a>
        <a href="https://sv-se.facebook.com/smakis.se" target="_blank">
          <div class="icon-container facebook"></div>
        </a>
      </nav>
    </div>
  </header>
</div>
