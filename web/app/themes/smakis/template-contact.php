<?php
/*
Template Name: Contact template
*/

?>


<section id="contact-top">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h2><?php the_field('contact_title'); ?></h2>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<p><?php the_field('contact_text'); ?></p>
			</div>
		</div>
	</div>
</section>

<section id="contact">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-sm-4 col-xs-12 left-info">
				<hr>
				<p class="title">Adress</p>
				<p><?php the_field('address_1'); ?></p>
				<p><?php the_field('address_2'); ?></p>
				<p><?php the_field('address_3'); ?></p>
				<p class="title">Besöksadress</p>
				<p><?php the_field('visit_address'); ?></p>
				<p class="title">Ring eller Maila</p>
				<p><?php the_field('phone_1'); ?></p>
				<p><?php the_field('phone_2'); ?></p>
				<p><?php the_field('email'); ?></p>
				<hr>
				<p class="title">Pressmaterial</p>
				<p>Här har vi samlat lite högupplösta <a href="<?php the_field('logo_link'); ?>">logotyper</a> för press.</p>
				<br>
				<p>Sätt upp dessa och motverka mobbingen i skolan. Ladda ner <a href="http://www.smakis.se/wp-content/themes/smakis/assets/img/posters/smakis_friends_affisch.zip">skolaffischer</a>.</p>
				<hr>

			</div>
			<div class="col-md-7 col-sm-8 col-xs-12">
				<?php gravity_form(1, false, false, false, '', true); ?>
			</div>
		</div>
	</div>
</section>

<?php if (get_field('about_title') != '' || get_field('about_text') != ''): ?>
	<section id="contact-top">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h2><?php the_field('about_title'); ?></h2>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<p><?php the_field('about_text'); ?></p>
				</div>
			</div>
		</div>
	</section>	
<?php endif ?>


<section id="about">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-push-2 col-xs-12">
				<p class="top-text"><?php the_field('about_text_1'); ?></p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-md-push-2 col-xs-12">
				<p class="bottom-text"><?php the_field('about_text_2'); ?></p>
			</div>
		</div>
	</div>
</section>

