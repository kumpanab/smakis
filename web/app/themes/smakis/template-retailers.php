<?php
/*
Template Name: Retailers template
*/

?>


<section id="retailers-top">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h2><?php the_field('title'); ?></h2>
				<p><?php the_field('text'); ?></p>
			</div>
		</div>
	</div>
</section>
<section id="retailers">
	<div class="container">
		<?php if( have_rows('shops') ): ?>
			<?php while( have_rows('shops') ): the_row();
				$color = get_sub_field('shop_color');
			?>
				<div class="row">
					<div class="col-xs-12"><h3><?php the_sub_field('shop_type'); ?></h3></div>
				</div>
				<div class="row">
					<?php if( have_rows('companys') ): ?>
						<?php while( have_rows('companys') ): the_row();?>
							<div class="col-lg-2 col-md-3 col-sm-4 col-xs-6">
								<div class="shop-container" style="background-color: #<?php echo $color; ?> ;">
									<p><?php the_sub_field('name'); ?></p>
								</div>
							</div>
						<?php endwhile; ?>
					<?php endif; ?>
				</div>
				<div class="row">
					<div class="col-xs-12"><hr></div>
				</div>
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
</section>
