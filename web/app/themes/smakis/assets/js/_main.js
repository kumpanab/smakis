/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 *
 * Google CDN, Latest jQuery
 * To use the default WordPress version of jQuery, go to lib/config.php and
 * remove or comment out: add_theme_support('jquery-cdn');
 * ======================================================================== */

(function($) {

// Use this variable to set up the common and page specific functions. If you
// rename this variable, you will also need to rename the namespace below.
var Roots = {
  // All pages
  common: {
    init: function() {
      // JavaScript to be fired on all pages

      function setMenuClick(event){
        var selector = $(event.data.element);
        if (selector.css('display') === 'none') {
          selector.show(0,
            function(){
              selector.toggleClass('clicked');
              selector.css('display', 'block');
              $('body').css('overflow', 'hidden');
            }
          );
        }
        else {
          selector.toggleClass('clicked');
        }
      }

      var set = $('.submenu:first').nextUntil(':not(.submenu)').andSelf();
      while (set.length > 0) {
        set = set.addBack().wrapAll('<div class="submenu-container"></div>').parent().nextAll('.submenu').first().nextUntil(':not(.submenu)');
      }
      $('.submenu-container').prev().addClass('submenu-category');
      $('.submenu-container').wrap('<div class="submenu-container2"></div>');
      $('.overlay-menu').css('display', 'block');
      $('.submenu-container').each(function(index){
        $(this).css('margin-top', '-' + ($(this).height() + 15) + 'px');
      });
      $('.overlay-menu').css('display', 'none');
      //$('.submenu-category').append('<div class="arrow"></div>');
      $('.submenu-category a').removeAttr('href');
      $('.submenu-category').click(function(){
        if ($(this).hasClass('open')) {
          $(this).next().children(":first").css('margin-top', '-' + ($(this).next().children(":first").height() + 11) + 'px');
          $(this).toggleClass('open');
        }
        else{
          $(this).next().children(":first").css('margin-top', '0px');
          $(this).toggleClass('open');
        }
      });

      var SvgIcon = svgIcon;
      var iconConfig = {
        hamburgerCross : {
          url : template_url + '/assets/img/menu/hamburger.svg',
          animation : [
            {
              el : 'path:nth-child(1)',
              animProperties : {
                from : { val : '{"path" : "m 5.0916789,16.818994 53.8166421,0"}' },
                to : { val : '{"path" : "M 12.972944,50.936147 51.027056,12.882035"}' }
              }
            },
            {
              el : 'path:nth-child(2)',
              animProperties : {
                from : { val : '{"transform" : "s1 1", "opacity" : 1}', before : '{"transform" : "s0 0"}' },
                to : { val : '{"opacity" : 0}' }
              }
            },
            {
              el : 'path:nth-child(3)',
              animProperties : {
                from : { val : '{"path" : "m 5.0916788,46.95698 53.8166422,0"}' },
                to : { val : '{"path" : "M 12.972944,12.882035 51.027056,50.936147"}' }
              }
            }
          ]
        }
      };

      var menuIcon = new SvgIcon(document.querySelector('.menu-button'), iconConfig, {speed: 70, size : { w : 30, h : 30 }} );

      FastClick.attach(document.body);

      $('#header-wrapper').height($('header').height());

      $('header').affix({
        offset: {
          top: function() { return $('#smakis-logo-container').height(); }
        }
      });

      $('.submenu').click(function(){
        $('.overlay-menu').toggleClass('clicked');
        $('body').css('overflow', 'visible');
        menuIcon.toggle();
      });

      $('.menu-button').click({element: '.overlay-menu'}, setMenuClick);

      $('.overlay-menu').on('webkitTransitionEnd oTransitionEnd transitionend msTransitionEnd', function() {
        if (!$('.overlay-menu').hasClass('clicked')) {
          $('.overlay-menu').css('display', 'none');
          $('body').css('overflow', 'visible');
        }
      });

      $(window).resize(function(){
        if ($('.menu-button').css('display') === 'none'){
          if ($('.overlay-menu').css('display') !== 'none'){
            $('.overlay-menu').css('display', 'none');
            $('.overlay-menu').toggleClass('clicked');
            $('body').css('overflow', 'visible');
          }
          if ($('.menu-button').length > 0) {
            $('.menu-button').remove();
          }
        }
        else{
          if ($('.menu-button').length < 1) {
            $('.overlay-menu').after('<span class="menu-button" data-icon-name="hamburgerCross"></span>');
            $('.menu-button').click({element: '.overlay-menu'}, setMenuClick);
            menuIcon = new SvgIcon(document.querySelector('.menu-button'), iconConfig, {speed: 70, size : { w : 30, h : 30 } } );
          }
        }
      });
      $('.promo-button').click(function(e){
        $('.promo-popup').fadeIn(400);
        $('html, body').animate({
            scrollTop: "0px"
        }, 800);
        e.preventDefault();
      });
      $('.promo-popup').click(function(){
        $('.promo-popup').fadeOut(200);
      });

    }
  },
  // Home page
  home: {
    init: function() {
      // JavaScript to be fired on the home page
      // $('.owl-carousel').owlCarousel({
      //     center: true,
      //     items:1,
      //     loop:true,
      //     margin:10,
      //     nav: true,
      //     dots: false,
      //     navSpeed: 1000,
      //     responsive:{
      //       768:{
      //         items:2
      //       }
      //     },
      //     mouseDrag: false,
      //     //touchDrag: false,
      //     //navText: ['<div class="chevron-container"><div class="chevron-left"></div></div>', '<div class="chevron-container"><div class="chevron-right"></div></div>']
      //     navText: ['<div class="chevron-container"><div class="chevron left"></div></div>', '<div class="chevron-container"><div class="chevron right"></div></div>']
      // });

      $('.owl-carousel').owlCarousel({
        center: true,
        items:1,
        loop:true,
        margin:10,
        nav: true,
        dots: false,
        navSpeed: 1000,
        mouseDrag: false,
        navText: ['<div class="chevron-container"><div class="chevron left"></div></div>', '<div class="chevron-container"><div class="chevron right"></div></div>']
      });

      $('.smoothie-carousel').owlCarousel({
        center: true,
        items:1,
        loop:true,
        margin:0,
        dots: false,
        navSpeed: 1000,
        mouseDrag: true,
        autoplay: true
      });

      $('#youtube-section iframe').css('width', ($('body').innerWidth() - 30 + 'px'));
      $('#youtube-section iframe').css('height', (($('body').innerWidth() - 30 ) / 1.77778) + 'px');
      $(window).resize(function(){
        $('#youtube-section iframe').css('width', ($('body').innerWidth() - 30 + 'px'));
        $('#youtube-section iframe').css('height', (($('body').innerWidth() - 30 ) / 1.77778) + 'px');
      });

    }
  },
  produkter: {
    init: function() {
      $('.product-container').click(function(){
        if ((window.innerWidth < 1200)){
          if ($(this).hasClass('clicked')) {
            $('.product-container').removeClass('clicked');
          }
          else{
            $('.product-container').removeClass('clicked');
            $(this).toggleClass('clicked');
          }
        }
      });
    }
  },
  plantera: {
    init: function() {
      $('.question').click(function(){
        $('.chevron-container span', this).toggleClass('bottom');
      });
      $('.owl-carousel').owlCarousel({
        center: true,
        items:1,
        loop:true,
        margin:10,
        nav: true,
        dots: false,
        navSpeed: 1000,
        mouseDrag: false,
        navText: ['<div class="chevron-container"><div class="chevron left"></div></div>', '<div class="chevron-container"><div class="chevron right"></div></div>']
      });

      $('#youtube-section iframe').css('width', ($('body').innerWidth() - 30 + 'px'));
      $('#youtube-section iframe').css('height', (($('body').innerWidth() - 30 ) / 1.77778) + 'px');
      $(window).resize(function(){
        $('#youtube-section iframe').css('width', ($('body').innerWidth() - 30 + 'px'));
        $('#youtube-section iframe').css('height', (($('body').innerWidth() - 30 ) / 1.77778) + 'px');
      });
      $('#header-wrapper').height(0);

    }
  },
  // About us page, note the change from about-us to about_us.
  about_us: {
    init: function() {
      // JavaScript to be fired on the about us page
    }
  }
};

// The routing fires all common scripts, followed by the page specific scripts.
// Add additional events for more control over timing e.g. a finalize event
var UTIL = {
  fire: function(func, funcname, args) {
    var namespace = Roots;
    funcname = (funcname === undefined) ? 'init' : funcname;
    if (func !== '' && namespace[func] && typeof namespace[func][funcname] === 'function') {
      namespace[func][funcname](args);
    }
  },
  loadEvents: function() {
    UTIL.fire('common');

    $.each(document.body.className.replace(/-/g, '_').split(/\s+/),function(i,classnm) {
      UTIL.fire(classnm);
    });
  }
};

$(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
