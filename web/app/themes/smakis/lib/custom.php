<?php
/**
 * Custom functions
 */
//Allow to upload svg files
function cc_mime_types( $mimes ){
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter( 'upload_mimes', 'cc_mime_types' );
add_filter("gform_validation_message", "change_message", 10, 2);
function change_message($message, $form){
  return '';
}
