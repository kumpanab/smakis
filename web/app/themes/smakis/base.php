<?php get_template_part('templates/head'); ?>
<body data-spy="scroll" data-target="#friends-side-nav" <?php body_class(); ?>>

  <!--[if lt IE 8]>
    <div class="alert alert-warning">
      <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'roots'); ?>
    </div>
  <![endif]-->
  <script type="text/javascript"><!--
    var template_url = "<?php bloginfo('template_url'); ?>";
  // -->
  </script>

  <div class="promo-popup">
    <div class="text-container">
      <div><h2>Miljonkampens stödjare:</h2></div>
      <?php while( have_rows('promo_supporters', 4) ): the_row();?>
        <div><?php the_sub_field('supporter') ?></div>
      <?php endwhile; ?>
    </div>
  </div>

  <div class="overlay-menu">
    <div class="list-container">
      <?php wp_nav_menu( array('menu' => 'Primary Navigation' )); ?>
      <div class="icon-container-overlay">
        <a href="https://sv-se.facebook.com/smakis.se" target="_blank"><div id="facebook" class="icon"></div></a>
        <a href="http://instagram.com/smakis" target="_blank"><div id="instagram" class="icon"></div></a>
      </div>
    </div>
  </div>

  <div class="menu-back"> </div>
  <span class="menu-button" data-icon-name="hamburgerCross"></span>


  <div class="mainContainer">
    <?php //if (!is_page_template('template-plantera.php')): ?>
      <section id="smakis-logo-container">
        <img id="shadow" src="<?php bloginfo('template_url'); ?>/assets/img/header/logo_shadow.png">
        <a href="<?php echo home_url(); ?>"><img src="<?php bloginfo('template_url'); ?>/assets/img/header/logo_cloud.svg"></a>
      </section>
    <?php //endif ?>

    <?php
      do_action('get_header');
      // Use Bootstrap's navbar if enabled in config.php
      if (current_theme_supports('bootstrap-top-navbar')) {
        get_template_part('templates/header-top-navbar');
      } else {
        get_template_part('templates/header');
      }
    ?>

    <?php if( is_page_template('template-homepage.php') || is_page_template('template-products.php') || is_page_template('template-friends.php') || is_page_template('template-retailers.php') || is_page_template('template-contact.php') || is_page_template('template-plantera.php') || is_page_template('template-foreningserbjudande.php')): ?>
      <?php include roots_template_path(); ?>
    <?php else: ?>
      <div class="wrap container" role="document">
        <div class="content row">
          <main class="main <?php echo roots_main_class(); ?>" role="main">
            <?php include roots_template_path(); ?>
          </main><!-- /.main -->
          <?php if (roots_display_sidebar()) : ?>
            <aside class="sidebar <?php echo roots_sidebar_class(); ?>" role="complementary">
              <?php include roots_sidebar_path(); ?>
            </aside><!-- /.sidebar -->
          <?php endif; ?>
        </div><!-- /.content -->
      </div><!-- /.wrap -->
    <?php endif; ?>

    <?php get_template_part('templates/footer'); ?>
  </div>
</body>
</html>
