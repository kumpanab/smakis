<?php
/*
Template Name: Friends template
*/

?>


<section id="friends-top">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h2><?php the_field('top_title'); ?></h2>
				<p><?php the_field('top_text'); ?></p>
			</div>
		</div>
	</div>
</section>


<?php if( have_rows('friends') ):
	while( have_rows('friends') ) {
		the_row();
		$menuList[] = get_sub_field('anchor');
	}
?>
	<section id="friends">
		<div class="container">
			<?php $i = 1; while( have_rows('friends') ): the_row();
				$friends_image = get_sub_field('friends_image');
			?>
				<div class="row">
					<div class="col-md-3 col-sm-4 hidden-xs">
						<?php if ($i == 1): ?>
							<div id="friends-side-nav" data-spy="affix" data-offset-top="600">
								<ul class="nav">
									<?php foreach($menuList as $link): ?>
										<li><a href="#<?php echo strtolower(str_replace(' ', '-', $link)); ?>"><?php echo $link; ?><span>›</span></a></li>	
									<?php endforeach; ?>
								</ul>
							</div>
						<?php endif ?>
					</div>
					<div class="col-lg-7 col-lg-push-0 col-md-8 col-md-push-1 col-sm-8 col-xs-12">
						<?php $anchor = get_sub_field('anchor');?>
						<div class="friend-container">
							<h3><span id="<?php echo strtolower(str_replace(' ', '-', $anchor));?>" class="anchor"></span><?php the_sub_field('friends_title'); ?></h3>
							<img src="<?php echo $friends_image['url']; ?>">
							<div class="top-text"><?php the_sub_field('friends_text_1'); ?></div>
							<div class="bottom-text"><?php the_sub_field('friends_text_2'); ?></div>
							<hr>
						</div>
					</div>
				</div>
			<?php $i++; endwhile; ?>
		</div>
	</section>
<?php endif; ?>