<?php
/*
Template Name: Foreningserbjudande template
*/

?>


<section id="refrigerator">
  <img class="top-border" src="<?php bloginfo('template_url'); ?>/assets/img/refrigerator/refrigerator_back.jpg">
	<div class="container drag-up">
		<div class="row">
			<div class="col-xs-12">
				<h2>KÖP SMAKIS EKO<br>föreningspall så får du</h2>
        <h1>KYLSKÅPET<br>PÅ KÖPET!</h1>
			</div>
		</div>
    <div class="row">
      <div class="col-xs-push-0 col-xs-12 col-md-push-2 col-md-8">
        <p class="refrigerator-top-text">När kylskåpet är tomt har ni tjänat mellan 15.000-20.000 kronor. Självklart kan ni sedan fortsätta beställa Smakis från er lokala grossist.</p>
      </div>
    </div>

		<div class="row">
			<div class="col-xs-12 col-sm-4">
        <div class="refrigerator-image">
          <img src="<?php bloginfo('template_url'); ?>/assets/img/refrigerator/372kyl_Smakis_EKO-forening_small.jpg">
          <a href="<?php bloginfo('template_url'); ?>/assets/img/refrigerator/372kyl_Smakis_EKO-forening.jpg">
            <img src="<?php bloginfo('template_url'); ?>/assets/img/refrigerator/magnify.jpg">
            <span>Förstora bild</span>
          </a>
        </div>
			</div>
      <div class="col-xs-12 col-sm-6">
        <p class="top-title">I EKO DRYCKESPAKETET INGÅR:</p>
        <div class="flex-number-box">
          <div class="flex-title">SMAKIS EKO-FRUKTDRYCKER</div>
          <div class="main-container"><span class="other-content">Apelsin</span><span class="center-content"></span><span class="other-content">20 krt x27x25cl</span></div>
          <div class="main-container"><span class="other-content">Päron</span><span class="center-content"></span><span class="other-content">20 krt x27x25cl</span></div>
          <div class="main-container"><span class="other-content">Jordgubb</span><span class="center-content"></span><span class="other-content">20 krt x27x25cl</span></div>

          <div class="flex-title">SMOOTHIE EKO-MELLIS</div>
          <div class="main-container"><span class="other-content">Jordgubb/Banan</span><span class="center-content"></span><span class="other-content">10 krt x18x25cl</span></div>
          <div class="main-container"><span class="other-content">Mango/Päron</span><span class="center-content"></span><span class="other-content">10 krt x18x25cl</span></div>
          <div class="main-container"><span class="other-content">Blåbär/Banan</span><span class="center-content"></span><span class="other-content">10 krt x18x25cl</span></div>

          <div class="flex-title">SMAKIS EKO-CHOKLADMJÖLK</div>
          <div class="main-container"><span class="other-content">Chokladmjölk</span><span class="center-content"></span><span class="other-content">6 krt x18x25 cl</span></div>
        </div>
      </div>
		</div>

      <div class="row total-price">
        <div class="col-xs-12 col-sm-5 col-md-6">
          <p class="top-title">Pris för hela EKO-paketet:</p>
        </div>
        <div class="col-xs-12 col-sm-7 col-md-6">
          <p class="price">9.990:-<span> + moms</span></p>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <p class="red-cursive">OCH KYLSKÅPET PÅ KÖPET!</p>
        </div>
      </div>
  </div>
  <div class="container">
    <div class="pink-container">
      <div class="row">
        <div class="col-xs-12">
          <p>Förslag på intäkter till föreningen:</p>
        </div>
      </div>

      <div class="row">
        <div class="col-xs-4">
          <p>Säljförslag</p>
        </div>
        <div class="col-xs-4">
          <p class="text-center">Alt 1</p>
        </div>
        <div class="col-xs-4">
          <p class="text-right">Alt 2</p>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-4">
          <p>Smakis 25 cl</p>
        </div>
        <div class="col-xs-4">
          <p class="text-center">10:-/st</p>
        </div>
        <div class="col-xs-4">
          <p class="text-right">12:-/st</p>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-4">
          <p>Smoothie 25 cl</p>
        </div>
        <div class="col-xs-4">
          <p class="text-center">15:-/st</p>
        </div>
        <div class="col-xs-4">
          <p class="text-right">18:-/st</p>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-4">
          <p>Chokladmjölk 25 cl</p>
        </div>
        <div class="col-xs-4">
          <p class="text-center">12:-/st</p>
        </div>
        <div class="col-xs-4">
          <p class="text-right">15:-/st</p>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-4">
          <p>Intäkter:</p>
        </div>
        <div class="col-xs-4">
          <p class="big-price">25.596:-</p>
        </div>
        <div class="col-xs-4">
          <p class="big-price text-right" class="big-price">30.780:-</p>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="refrigerator-form">
  <div class="container">

    <div class="row">
      <div class="col-xs-push-0 col-xs-12 col-md-push-2 col-md-8">
        <p>
          -Ja, jag vill beställa<br>Smakis EKO-föreningspall och få kylskåpet på köpet!
        </p>
        <img src="<?php bloginfo('template_url'); ?>/assets/img/refrigerator/arrow.png">
      </div>
    </div>

    <div class="row">
      <div class="col-xs-push-0 col-xs-12 col-md-push-2 col-md-8">
        <?php gravity_form(5, false, false, false, '', true); ?>
      </div>
    </div>

    <div class="row">
      <div class="col-xs-12">
        <p>Din beställning kommer skickas till:</p>
        <p>Nordic Cooling Solutions AB, Järnvägsgatan 7, 731 30 Köping</p>
        <p>DRYCKEN LEVERERAS MED KYLSKÅPET!</p>
      </div>
    </div>

  </div>
  <img class="bottom-border" src="<?php bloginfo('template_url'); ?>/assets/img/refrigerator/refrigerator_back2.jpg">
</section>
