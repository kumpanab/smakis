<?php
/*
Template Name: Home page template
*/

?>

<?php if (get_field('promo_1_title') != ''): ?>
<section id="promo-one">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h2><?php the_field('promo_1_title'); ?></h2>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<img src="<?php the_field('promo_1_image'); ?>">
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12 col-sm-push-0 col-md-8 col-md-push-2">
				<p><?php the_field('promo_1_text'); ?></p>
			</div>
		</div>
		<?php if( have_rows('promo_supporters') ): ?>
		<div class="row">
			<div class="col-xs-12">
				<a class="promo-button" href="#">Se vilka som stödjer oss!</a>
			</div>
		</div>
		<?php endif; ?>
	</div>
</section>
<?php endif ?>


<section id="slide-one">
	<h2><?php the_field('slide_1_title'); ?></h2>
	<?php if( have_rows('slide_1') ): ?>
		<div class="owl-carousel">
			<?php while( have_rows('slide_1') ): the_row();
				$slide1Image = get_sub_field('slide_1_image');
			?>
				<div>
					<p><?php echo get_sub_field('slide_1_text'); ?></p>
					<img src="<?php echo $slide1Image['url']; ?>">
				</div>
			<?php endwhile; ?>
		</div>
	<?php endif; ?>
</section>

<?php if( have_rows('slide_2') ): ?>
	<section id="slide-two">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div id="carousel-example-generic" class="carousel slide" data-interval="false">
						  <!-- Indicators -->
						  <ol class="carousel-indicators">
						  	<?php
						  		$i = 0;
						  		while( have_rows('slide_2') ):
							  		the_row();
									if ($i == 0) {
										echo '<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>';
									}
									else{
										echo '<li data-target="#carousel-example-generic" data-slide-to="'.$i.'"></li>';
									}
									$i++;
								endwhile; 
							?>
						  </ol>

						  <!-- Wrapper for slides -->
						  <div class="carousel-inner">
						  	<?php $i = 0; while( have_rows('slide_2') ): the_row();
								$slide2Image = get_sub_field('slide_2_image');
								if ($i == 0) {
									echo '<div class="item active">';
								}
								else{
									echo '<div class="item">';
								}
							?>
							    	<img src="<?php echo $slide2Image['url']; ?>">
							    </div>
							    <?php $i++; ?>
							<?php endwhile; ?>
						  </div>

						  <!-- Controls -->
						  <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
						    <span class="glyphicon glyphicon-chevron-left"></span>
						  </a>
						  <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
						    <span class="glyphicon glyphicon-chevron-right"></span>
						  </a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<a class="button" href="http://www.smakis.se/wp-content/themes/smakis/assets/img/posters/smakis_friends_affisch.zip">Ladda ner affischerna här</a>
				</div>
			</div>
		</div>
		</div>
	</section>
<?php endif; ?>

<section id="our-products">
	<?php $buttonFieldImage = get_field('button_field_image'); ?>
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h2><?php the_field('button_field_title'); ?></h2>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<p><?php the_field('button_field_text'); ?></p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<img src="<?php echo $buttonFieldImage['url']; ?>">
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<a class="button" href="<?php the_field('button_field_button_link'); ?>"><?php the_field('button_field_button_text'); ?></a>
			</div>
		</div>
	</div>
</section>
