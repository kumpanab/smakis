<?php
/*
Template Name: Plantera template
*/

?>

<section id="smoothie-top">
  <div class="container-fluid">
    <div class="row vertical-align">
      <div class="col-xs-12 col-sm-7 hidden-xs">
        <img src="<?php bloginfo('template_url'); ?>/assets/img/smoothie/smoothies.png">
      </div>
      <div class="col-xs-12 col-sm-5 hidden-xs">
        <p class="big">Smoothie<br>Eko-mellis</p>
        <p>Ett snällare mellanmål<br>för både dig och miljön.<br>Utan tillsatt socker.
      </div>
      <div class="col-xs-12 hidden-sm hidden-md hidden-lg">
        <p class="big">Smoothie<br>Eko-mellis</p>
        <p>Ett snällare mellanmål<br>för både dig och miljön.<br>Utan tillsatt socker.
        <img src="<?php bloginfo('template_url'); ?>/assets/img/smoothie/smoothies.png">
      </div>
    </div>
  </div>
</section>

<section id="plantera-top">
  <img src="<?php bloginfo('template_url'); ?>/assets/img/plantera/plantera_smakis_header.jpg" class="hidden-xs">
  <img src="<?php bloginfo('template_url'); ?>/assets/img/plantera/plantera-en-smakis-top_small.jpg" class="hidden-sm hidden-md hidden-lg">
</section>


<?php //if (get_field('promo_1_title') != ''): ?>
<section id="plantera-steps">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h2 class="with-break"><?php the_field('rubrik') ?></h2>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-xs-push-0 col-sm-8 col-sm-push-2 col-md-6 col-md-push-3">
        <p class="top-text">
          <?php the_field('text') ?>
        </p>
        <p>
          I samarbete med:
        </p>
			</div>
		</div>
    <div class="row">
      <div class="col-xs-12">
        <img class="logos" src="<?php bloginfo('template_url'); ?>/assets/img/plantera/logodelar.svg">
        <h2 class="first"><?php the_field('rubrik2') ?></h2>
      </div>
    </div>
    <div class="row hidden-xs">
      <div class="col-xs-12 col-xs-push-0 col-md-10 col-md-push-1">
        <div class="col-xs-12 col-sm-3">
          <img class="step-image" src="<?php bloginfo('template_url'); ?>/assets/img/plantera/steg_1.svg">
        </div>
        <div class="col-xs-12 col-sm-3">
          <img class="step-image" src="<?php bloginfo('template_url'); ?>/assets/img/plantera/steg_2.svg">
        </div>
        <div class="col-xs-12 col-sm-3">
          <img class="step-image" src="<?php bloginfo('template_url'); ?>/assets/img/plantera/steg_3.svg">
        </div>
        <div class="col-xs-12 col-sm-3">
          <img class="step-image" src="<?php bloginfo('template_url'); ?>/assets/img/plantera/steg_4.svg">
        </div>
        <div class="col-xs-12 col-sm-3">
          <img class="step-image top-margin" src="<?php bloginfo('template_url'); ?>/assets/img/plantera/steg_5.svg">
        </div>
        <div class="col-xs-12 col-sm-3">
          <img class="step-image top-margin" src="<?php bloginfo('template_url'); ?>/assets/img/plantera/steg_6.svg">
        </div>
        <div class="col-xs-12 col-sm-3">
          <img class="step-image top-margin" src="<?php bloginfo('template_url'); ?>/assets/img/plantera/steg_7.svg">
        </div>
        <div class="col-xs-12 col-sm-3">
          <img class="step-image top-margin" src="<?php bloginfo('template_url'); ?>/assets/img/plantera/steg_8.svg">
        </div>
      </div>
    </div>

    <div class="row hidden-sm hidden-md hidden-lg">
      <div class="col-xs-12">
        <div class="owl-carousel">
            <div>
              <img class="step-image" src="<?php bloginfo('template_url'); ?>/assets/img/plantera/steg_1.svg">
            </div>
            <div>
              <img class="step-image" src="<?php bloginfo('template_url'); ?>/assets/img/plantera/steg_2.svg">
            </div>
            <div>
              <img class="step-image" src="<?php bloginfo('template_url'); ?>/assets/img/plantera/steg_3.svg">
            </div>
            <div>
              <img class="step-image" src="<?php bloginfo('template_url'); ?>/assets/img/plantera/steg_4.svg">
            </div>
            <div>
              <img class="step-image" src="<?php bloginfo('template_url'); ?>/assets/img/plantera/steg_5.svg">
            </div>
            <div>
              <img class="step-image" src="<?php bloginfo('template_url'); ?>/assets/img/plantera/steg_6.svg">
            </div>
            <div>
              <img class="step-image" src="<?php bloginfo('template_url'); ?>/assets/img/plantera/steg_7.svg">
            </div>
            <div>
              <img class="step-image" src="<?php bloginfo('template_url'); ?>/assets/img/plantera/steg_8.svg">
            </div>
        </div>
      </div>
    </div>
	</div>
</section>
<?php //endif ?>

<section id="youtube-section">
  <iframe src="<?php the_field('youtube') ?>" frameborder="0" allowfullscreen></iframe>
</section>

<section id="instagram-top">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <img class="instagram-camera" src="<?php bloginfo('template_url'); ?>/assets/img/plantera/instagram.svg">
        <a href="http://instagram.com/smakis"><h2>#PlanteraEnSmakis</h2></a>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-xs-push-0 col-sm-10 col-sm-push-1 col-md-8 col-md-push-2 col-lg-6 col-lg-push-3">
        <p class="large">
          <?php the_field('insta_text') ?>
        </p>
        <a href="http://instagram.com/smakis" class="button">Följ oss</a>
      </div>
    </div>
  </div>
</section>

<section id="instagram-feed-smakis" class="hidden-xs">
  <?php echo do_shortcode('[instagram-feed num=6 cols=3]'); ?>
</section>

<section id="instagram-feed-smakis" class="hidden-sm hidden-md hidden-lg">
  <?php echo do_shortcode('[instagram-feed num=6 cols=3]'); ?>
</section>

<section id="faq">
	<div class="container">
		<div class="row">
      <div class="col-xs-12 col-xs-push-0 col-sm-8 col-sm-push-2 top">
        <h2>Vanliga frågor</p>
        <p><?php the_field('faq_text') ?></p>
      </div>
    </div>
    <div class="row faq-row">
      <?php $faqBlocks = array();while ( have_rows('faq') ) : the_row(); ?>
        <?php array_push($faqBlocks, array(get_sub_field('question'), get_sub_field('answer'))); ?>
      <?php endwhile; ?>
      <div class="col-xs-12 col-sm-6 faq-block">
        <?php for ($i=0; $i < (count($faqBlocks) / 2); $i++) {
          echo '
            <p class="question" data-toggle="collapse" data-target="#faq-answer-'.$i.'">'.$faqBlocks[$i][0].'<span class="chevron-container"><span class="chevron bottom"></span></span></p>
            <p id="faq-answer-'.$i.'" class="collapse answer"><span>'.$faqBlocks[$i][1].'</span></p>
          ';
        } ?>
      </div>
      <div class="col-xs-12 col-sm-6 faq-block">
        <?php for ($i=$i; $i < count($faqBlocks); $i++) {
          echo '
            <p class="question" data-toggle="collapse" data-target="#faq-answer-'.$i.'">'.$faqBlocks[$i][0].'<span class="chevron-container"><span class="chevron bottom"></span></span></p>
            <p id="faq-answer-'.$i.'" class="collapse answer"><span>'.$faqBlocks[$i][1].'</span></p>
          ';
        } ?>
      </div>
		</div>
    <div class="row">
      <div class="col-xs-12 col-xs-push-0 col-sm-6 col-sm-push-6">
        <img class="strawberry" src="<?php bloginfo('template_url'); ?>/assets/img/plantera/jordgubbe.jpg">
      </div>
	</div>
</section>

